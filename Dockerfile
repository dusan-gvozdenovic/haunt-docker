FROM alpine:3.7
MAINTAINER Dušan Gvozdenović

RUN apk add build-base \
            autoconf libtool automake \
            gmp gmp-dev \
            libunistring libunistring-dev \
            libffi libffi-dev \
            gc-dev \
            gperf \
            gettext gettext-dev \
            git \
            texinfo \
            gzip

WORKDIR /tmp

ADD https://ftp.gnu.org/gnu/guile/guile-3.0.2.tar.xz .

RUN tar xvf guile-3.0.2.tar.xz; \
    cd guile-3.0.2; \
    ./configure --prefix=/usr; \
    make; \
    make install; \
    cd ..; \
    rm -rf guile-3.0.2*

ADD https://github.com/OrangeShark/guile-commonmark/releases/download/v0.1.2/guile-commonmark-0.1.2.tar.gz .

RUN tar xvzf guile-commonmark-0.1.2.tar.gz; \
    cd guile-commonmark-0.1.2; \
    sed -e 's/GUILE_PKG(\[\(.*\)\])/GUILE_PKG([3.0 \1])/g' -i configure.ac; \
    autoreconf; \
    ./configure --prefix=/usr; \
    make; \
    make install; \
    cd ..; \
    rm -rf guile-commonmark-0.1.2*

RUN git clone git://git.sv.gnu.org/guile-reader.git; \
    cd guile-reader; \
    mkdir -p build-aux && touch build-aux/config.rpath; \
    sed -e 's/GUILE_PKG(\[\(.*\)\])/GUILE_PKG([3.0 \1])/g' -i configure.ac; \
    sed -e 's/\(<compat.h>\)/\1\n#ifndef EOF\n#define EOF -1\n#endif\n/g' -i src/compat.c; \
    autoreconf -i --force --verbose; \
    CFLAGS= CPPFLAGS= LDFLAGS= GUILE_SITE=/usr/share/guile/site ./configure --prefix=/usr --with-guilemoduledir=/usr/share/guile/site; \
    make; \
    make install; \
    cd ..; \
    rm -rf guile-reader*

ADD https://files.dthompson.us/guile-syntax-highlight/guile-syntax-highlight-0.1.tar.gz /tmp

RUN tar xvzf guile-syntax-highlight-0.1.tar.gz; \
    cd guile-syntax-highlight-0.1; \
    sed -e 's/GUILE_PKG(\[\(.*\)\])/GUILE_PKG([3.0 \1])/g' -i configure.ac; \
    autoreconf; \
    ./configure --prefix=/usr; \
    make; \
    make install; \
    cd ..; \
    rm -rf guile-syntax-highlight-0.1*

ADD https://files.dthompson.us/haunt/haunt-0.2.4.tar.gz /tmp

RUN tar xvzf haunt-0.2.4.tar.gz; \
    cd haunt-0.2.4; \
    sed -e 's/GUILE_PKG(\[\(.*\)\])/GUILE_PKG([3.0 \1])/g' -i configure.ac; \
    autoreconf; \
    ./configure --prefix=/usr; \
    make; \
    make install; \
    cd ..; \
    rm -rf haunt-0.2.4*

RUN rm -rf /tmp/*

RUN mkdir -p /app

WORKDIR /app

EXPOSE 8080
